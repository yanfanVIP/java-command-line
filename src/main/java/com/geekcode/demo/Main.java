package com.geekcode.demo;

import com.geekcode.guide.Guide;
import com.geekcode.guide.annotation.GuiMethod;
import com.geekcode.guide.annotation.GuiType;
import com.geekcode.guide.annotation.Param;

@GuiType(
	name = "测试环境1",
	message = {
		"欢迎使用GUISystem",
		"请在下列提示中选择操作"
	}
)
public class Main{
	
	public static void main(String[] args) throws Exception {
		Guide.run(Main.class);
	}
	
	@GuiMethod(name="ECHO输出", message= {"ECHO输出","输入姓名，输出[hello {姓名}]"})
	public void echo(@Param(message="请输入姓名") String name) {
		System.out.println("hello " + name);
	}
	
	@GuiMethod(name="AI对话", message= {"最简单的AI对话系统"})
	public void ai(@Param(message="请输入") String message) {
		System.out.println(message.replace("你", "我").replace("?", "").replace("？", "").replace("吗", ""));
	}
}
